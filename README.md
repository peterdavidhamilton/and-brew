# AND Digital Brewfile

<https://github.com/Homebrew/homebrew-bundle>

__Goals:__

* speed up bootstrapping laptops during onboarding
* encourage configuration in version control

__Outcomes:__

* restoring wiped computers becomes easier
* opportunity to recommend software

__TODO:__

* guidance from tech-support regarding use of AppStore ?
* org/club/squad specific template tailored to roles ?

